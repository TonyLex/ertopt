#ifndef NODE_H
#define NODE_H

#include <QVector3D>
#include <QObject>

struct LimitsAndSteps{
    qreal X_min = -10;
    qreal Y_min = -10;
    qreal Z_min = 0;

    qreal X_max = 10;
    qreal Y_max = 10;
    qreal Z_max = 10;

    qreal step_x = 1;
    qreal step_y = 1;
    qreal step_z = 1;
};

enum BorderType{
    internal, // Узел лежит внутри области
    bottom,// Узел лежит на нижней границе, исключая ребра и углы
    t_b_r_corner //

};

class Node: public QObject
{
    Q_OBJECT
public:

    Node(QVector3D coord_l):
        coord(coord_l)/*,
        left(nullptr), right(nullptr),
        top(nullptr), bottom(nullptr),
        front(nullptr), back(nullptr)*/
    {

    };

    /// координаты точки (x,y,z)
    QVector3D coord;

    /// Проводимость
    qreal cond = 0;

    /// Потенциал в узле
    qreal fi = 0;

    /// Ток в узле (если в этом узле нет электрода, то и тока не будет)
    qreal I = 0;

    /// Определяет, где лежит узел
    int border_type = 0;

    /// Коэффициент связи узла
    qreal C_self = 0;

    /// Коэффициент связи Левого соседа в узле (-y)
    qreal C_left = 0;

    /// Коэффициент связи Правого соседа в узле (+y)
    qreal C_right = 0;

    /// Коэффициент связи Верхнего соседа в узле (-z)
    qreal C_top = 0;

    /// Коэффициент связи Нижнего соседа в узле (+z)
    qreal C_bottom = 0;

    /// Коэффициент связи Переднего соседа в узле (+y)
    qreal C_front = 0;

    /// Коэффициент связи Заднего соседа в узле (-y)
    qreal C_back = 0;

    qreal cond_im1_jm1_km1;
    qreal cond_i_jm1_km1;
    qreal cond_im1_j_km1;
    qreal cond_im1_jm1_k;

    qreal cond_im1_j_k;
    qreal cond_i_jm1_k;
    qreal cond_i_j_km1;

    /// Расчет Коэффициента связи узла
    void calculate_C_self();

    /// Расчет Коэффициента связи Левого соседа в узле (-y)
    void calculate_C_left();

    /// Расчет Коэффициент связи Правого соседа в узле (+y)
    void calculate_C_right();

    /// Расчет Коэффициент связи Верхнего соседа в узле (-z)
    void calculate_C_top();

    /// Расчет Коэффициент связи Нижнего соседа в узле (+z)
    void calculate_C_bottom();

    /// Расчет Коэффициент связи Переднего соседа в узле (+y)
    void calculate_C_front();

    /// Расчет Коэффициент связи Заднего соседа в узле (-y)
    void calculate_C_back();

    /// Расчет всех Коэффициентов связи в самом узле и соседних
    void calculate_all_C();

    /*
    /// Левый сосед в узле (-y)
    Node * left = nullptr;//new Node({0,0,0});

    /// Правый сосед в узле (+y)
    Node * right = nullptr;

    /// Верхний сосед в узле (-z)
    Node * top = nullptr;

    /// Нижний сосед в узле (+z)
    Node * bottom = nullptr;

    /// Передний сосед в узле (+y)
    Node * front = nullptr;

    /// Задний сосед в узле (-y)
    Node * back = nullptr;
    */

    LimitsAndSteps limits;

signals:
    void signal_NodeCreated(QVector3D pos, qreal cond);

};

#endif // NODE_H
