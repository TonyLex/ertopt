#include "Grid.h"

Grid::Grid()
{

}

QList<QList<QList<Node *>>> &Grid::getNodeList()
{
    return NodeList;
}

void Grid::setLimits(LimitsAndSteps limits_l)
{
    limits = limits_l;
}

void Grid::createGrid()
{
    qreal cond_default = 1;

    for(qreal x = limits.X_min; x<limits.X_max; x+=limits.step_x){
        QList<QList<Node *>> X_list;
        for(qreal y = limits.Y_min; y<limits.Y_max; y+=limits.step_y){
            QList<Node *> Y_list;
            for(qreal z = limits.Z_min; z<limits.Z_max; z+=limits.step_z){
                Y_list.push_back(nullptr);
            }
            X_list.push_back(Y_list);
        }
        NodeList.push_back(X_list);
    }

    int i = 0;
    for(qreal x = limits.X_min; x<limits.X_max; x+=limits.step_x){
        QList<QList<Node *>> X_list;
        int j = 0;
        for(qreal y = limits.Y_min; y<limits.Y_max; y+=limits.step_y){
            QList<Node *> Y_list;
            int k = 0;
            for(qreal z = limits.Z_min; z<limits.Z_max; z+=limits.step_z){

                Node *node = new Node(QVector3D(x,y,z));
                QList<Shape*> shapesList = ShapeManager::getInstance()->getShapesList();

                node->cond = cond_default;

                for(auto &shape:shapesList){
                    if(shape->intersectPoint(node->coord))
                        node->cond = shape->getCond();
                }

                if(z == limits.Z_max){
                    // Узел находится внизу (Z_max)
                    node->border_type = BorderType::bottom;
                } else if(x == limits.X_min || y == limits.Y_min){
                    // Узел находится справа или сзади (X_min, Y_min)
                    node->border_type = BorderType::bottom;
                }

                if(x != limits.X_min && y != limits.Y_min && z != limits.Z_min){
                    qDebug()<<i<<'\t'<<j<<'\t'<<k<<'\t' <<node->coord;
                    if(NodeList[i-1][j-1][k-1]!=nullptr){
                        node->cond_im1_jm1_km1 = NodeList[i-1][j-1][k-1]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }

                    if(NodeList[i][j-1][k-1]!=nullptr){
                        node->cond_i_jm1_km1 = NodeList[i][j-1][k-1]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }
                    if(NodeList[i-1][j][k-1]!=nullptr){
                        node->cond_im1_j_km1 = NodeList[i-1][j][k-1]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }
                    if(NodeList[i-1][j-1][k]!=nullptr){
                        node->cond_im1_jm1_k = NodeList[i-1][j-1][k]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }
                    if(NodeList[i-1][j][k]!=nullptr){
                        node->cond_im1_j_k = NodeList[i-1][j][k]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }

                    if(NodeList[i][j-1][k]!=nullptr){
                        node->cond_i_jm1_k = NodeList[i][j-1][k]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }
                    if(NodeList[i][j][k-1]!=nullptr){
                        node->cond_i_j_km1 = NodeList[i][j][k-1]->cond;
                    } else{
                        qDebug()<<"nullptr";
                    }

                }

                NodeList[i][j][k];
                k++;
            }
            j++;
        }
        i++;
    }

    /*
    for(auto &node_1:NodeList){
        for(auto &node_2:NodeList){
            qreal cond_im1_jm1_km1;
            qreal cond_i_jm1_km1;
            qreal cond_im1_j_km1;
            qreal cond_im1_jm1_k;

            qreal cond_im1_j_k;
            qreal cond_i_jm1_k;
            qreal cond_i_j_km1;

            if(node_2->coord == node_1->coord+QVector3D(-limits.step_x,0,0))
                node_1->left = node_2;

            if(node_2->coord == node_1->coord+QVector3D(limits.step_x,0,0))
                node_1->right = node_2;

            if(node_2->coord == node_1->coord+QVector3D(0,0,-limits.step_z))
                node_1->top = node_2;

            if(node_2->coord == node_1->coord+QVector3D(0,0,limits.step_z))
                node_1->bottom = node_2;

            if(node_2->coord == node_1->coord+QVector3D(0,limits.step_y,0))
                node_1->front = node_2;

            if(node_2->coord == node_1->coord+QVector3D(0,-limits.step_y,0))
                node_1->back = node_2;

            if(node_1->left!=nullptr && node_1->right!=nullptr &&
               node_1->top!=nullptr && node_1->bottom!=nullptr &&
               node_1->left!=nullptr && node_1->left!=nullptr)
                    break;

        }

    }
    */


}

qreal Grid::getCond_default()
{
    return cond_default;
}

void Grid::setCond_default(qreal cond)
{
    cond_default = cond;
}
