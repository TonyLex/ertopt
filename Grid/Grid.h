#ifndef GRID_H
#define GRID_H

#include <Grid/Node.h>
#include <GraphicEditor3D/Shapes/ShapeManager.h>



class Grid
{
public:
    Grid();

    QList<QList<QList<Node *>>> &getNodeList();
    void setLimits(LimitsAndSteps limits);
    void createGrid();

    qreal getCond_default();
    void setCond_default(qreal cond);


private:
    QList<QList<QList<Node *>>> NodeList;
    LimitsAndSteps limits;

    qreal cond_default = 0;


};

#endif // GRID_H
