#include "MainWidget.h"

MainWidget::MainWidget(QWidget *parent)
    :QWidget(parent)
{
    stackWidgets = new QStackedWidget();

    graph_editor = new GraphEditorWidget();
    setting = new SettingsWidget();

    stackWidgets->addWidget(graph_editor);
    stackWidgets->addWidget(setting);

    ButtonOpenGraphEditor = new CommonButton(":/Images/open graph editor.png", tr("Graphic editor"));
    ButtonOpenSettings = new CommonButton(":/Icons/open settings module.png", tr("Settings"));


    QVBoxLayout *stackWidgetLayout = new QVBoxLayout;
    stackWidgetLayout->addWidget(stackWidgets);

    mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(createMainButtons());
    mainLayout->addLayout(stackWidgetLayout);

}

void MainWidget::openGraphEditor()
{
    stackWidgets->setCurrentWidget(graph_editor);
}

void MainWidget::openSettings()
{
    stackWidgets->setCurrentWidget(setting);
}

QHBoxLayout *MainWidget::createMainButtons()
{
    QHBoxLayout *mainButtonsLayout = new QHBoxLayout(this);
    mainButtonsLayout->addWidget(ButtonOpenGraphEditor);
    mainButtonsLayout->addWidget(ButtonOpenSettings);

    mainButtonsLayout->addItem(new QSpacerItem(1000,0,QSizePolicy::Expanding, QSizePolicy::Fixed));

    connect(ButtonOpenGraphEditor, &QPushButton::clicked, this, &MainWidget::openGraphEditor);
    connect(ButtonOpenSettings, &QPushButton::clicked, this, &MainWidget::openSettings);

    return mainButtonsLayout;
}
