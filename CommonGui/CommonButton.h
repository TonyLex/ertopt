#ifndef COMMONBUTTON_H
#define COMMONBUTTON_H

#include <QPushButton>
#include <QString>
#include <QMouseEvent>
#include <QLabel>
#include <QDebug>

class CommonButton: public QPushButton
{
    Q_OBJECT
public:
    CommonButton(QString icon_path_l, QString title_l);
    void makeDarkBackground();
    void makeGrayBackground();

private:
    QString icon_path = "";
    QSize icon_size = {25,25};
    QString title = "";
    QLabel *LabelTitle;


};

#endif // COMMONBUTTON_H
