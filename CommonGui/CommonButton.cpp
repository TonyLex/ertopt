#include "CommonButton.h"

CommonButton::CommonButton(QString icon_path_l, QString title_l)
{
    this->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    icon_path = icon_path_l;
    QIcon icon(icon_path);
    this->setIcon(icon);
    this->setIconSize(icon_size);

    title = title_l;
    LabelTitle = new QLabel(title);
    setToolTip(title);
}

void CommonButton::makeDarkBackground()
{
    setStyleSheet("* { background-color: rgb(100,100,100) }");
}

void CommonButton::makeGrayBackground()
{
    setStyleSheet("* { background-color: rgb(230,230,230) }");
}
