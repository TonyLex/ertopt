#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <GraphicEditor3D/GraphEditorWidget.h>
#include <QBoxLayout>
#include <QPushButton>
#include <QSpacerItem>
#include <CommonGui/CommonButton.h>
#include <Settings/SettingsWidget.h>
#include <QStackedWidget>

class MainWidget: public QWidget
{
    Q_OBJECT
public:
    MainWidget(QWidget *parent = nullptr);

private slots:
    void openGraphEditor();
    void openSettings();

private:
    QStackedWidget *stackWidgets;

    GraphEditorWidget *graph_editor;
    SettingsWidget *setting;

    CommonButton *ButtonOpenGraphEditor;
    CommonButton *ButtonOpenSettings;

    QVBoxLayout *mainLayout;

    QHBoxLayout * createMainButtons();
};

#endif // MAINWIDGET_H
