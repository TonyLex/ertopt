#ifndef SOLVEDIRECTPROBLEMFDM_H
#define SOLVEDIRECTPROBLEMFDM_H


#include <iostream>
#include <vector>
#include <unordered_map>
#include <eigen-3.4.0/Eigen/Dense>
#include <eigen-3.4.0/Eigen/Sparse>
#include <eigen-3.4.0/Eigen/QR>
#include <QDebug>

using namespace std;
using namespace Eigen;

struct BoundaryCondition {
    int node;
    double value;
    // type of boundary condition (0: potential, 1: current)
    int type;
};

class SolveDirectProblemFDM {
private:
    int x_dim, y_dim, z_dim;
    int x_ele, y_ele, z_ele;
    double dx, dy, dz;
    vector<BoundaryCondition> boundary_conditions;
    MatrixXd global_conductance;
    VectorXd global_current;

public:
    SolveDirectProblemFDM(int x_dim, int y_dim, int z_dim, int x_ele, int y_ele, int z_ele)
        : x_dim(x_dim), y_dim(y_dim), z_dim(z_dim), x_ele(x_ele), y_ele(y_ele), z_ele(z_ele) {
        dx = x_dim / x_ele;
        dy = y_dim / y_ele;
        dz = z_dim / z_ele;
    }

    void setBoundaryConditions(const vector<BoundaryCondition>& bcs) {
        boundary_conditions = bcs;
    }

    VectorXd solve(double resistivity) {
        global_conductance = MatrixXd::Zero(x_ele * y_ele * z_ele, x_ele * y_ele * z_ele);
        global_current = VectorXd::Zero(x_ele * y_ele * z_ele);
        assemble_global_matrices(resistivity);

        ColPivHouseholderQR<MatrixXd> solver(global_conductance);
        VectorXd potential = solver.solve(global_current);
        return potential;
    }

    void assemble_global_matrices(double resistivity) {
        for (int k = 0; k < z_ele; k++) {
            for (int j = 0; j < y_ele; j++) {
                for (int i = 0; i < x_ele; i++) {
                    int node = k * (x_ele * y_ele) + j * x_ele + i;
                    if (i > 0) {
                        global_conductance.coeffRef(node, node - 1) = -1 / dx / dx;
                        global_conductance.coeffRef(node - 1, node) = -1 / dx / dx;
                    }
                    if (i < x_ele - 1) {
                        global_conductance.coeffRef(node, node + 1) = -1 / dx / dx;
                        global_conductance.coeffRef(node + 1, node) = -1 / dx / dx;
                    }
                    if (j > 0) {
                        global_conductance.coeffRef(node, node - x_ele) = -1 / dy / dy;
                        global_conductance.coeffRef(node - x_ele, node) = -1 / dy / dy;
                    }
                    if (j < y_ele - 1) {
                        global_conductance.coeffRef(node, node + x_ele) = -1 / dy / dy;
                        global_conductance.coeffRef(node + x_ele, node) = -1 / dy / dy;
                    }
                    if (k > 0) {
                        global_conductance.coeffRef(node, node - x_ele * y_ele) = -1 / dz / dz;
                        global_conductance.coeffRef(node - x_ele * y_ele, node) = -1 / dz / dz;
                    }
                    if (k < z_ele - 1) {
                        global_conductance.coeffRef(node, node + x_ele * y_ele) = -1 / dz / dz;
                        global_conductance.coeffRef(node + x_ele * y_ele, node) = -1 / dz / dz;
                    }
                    global_conductance.coeffRef(node, node) = -2 * (1 / dx / dx + 1 / dy / dy + 1 / dz / dz);
                    global_current[node] = resistivity;
                }
            }
        }

        for (auto bc : boundary_conditions) {
            if (bc.type == 0) {
                global_current[bc.node] = bc.value;
                global_conductance.row(bc.node) = VectorXd::Zero(x_ele * y_ele * z_ele);
                global_conductance.col(bc.node) = VectorXd::Zero(x_ele * y_ele * z_ele);
                global_conductance.coeffRef(bc.node, bc.node) = 1;
            }
            else {
                global_current[bc.node] += bc.value;
            }
        }
    }
};



#endif // SOLVEDIRECTPROBLEMFDM_H
