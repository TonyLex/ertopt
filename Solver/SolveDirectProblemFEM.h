#include <iostream>
#include <vector>
#include <unordered_map>
#include <eigen-3.4.0/Eigen/Dense>
#include <eigen-3.4.0/Eigen/Sparse>
#include <eigen-3.4.0/Eigen/QR>
#include <QDebug>

using namespace std;
using namespace Eigen;

#include <vector>
using namespace std;
using namespace Eigen;

/*
struct BoundaryCondition {
    int node;
    double value;
    // type of boundary condition (0: potential, 1: current)
    int type;
};

class SolveDirectProblemFEM {
public:
    SolveDirectProblemFEM(double x_dim, double y_dim, double z_dim, int x_ele, int y_ele, int z_ele)
        : x_dim(x_dim), y_dim(y_dim), z_dim(z_dim), x_ele(x_ele), y_ele(y_ele), z_ele(z_ele) {
        num_nodes = (x_ele + 1) * (y_ele + 1) * (z_ele + 1);
        num_elements = x_ele * y_ele * z_ele;
        connectivity = computeConnectivity();
        nodes = computeNodes();
    }

    void setBoundaryConditions(const vector<BoundaryCondition> &boundary_conditions) {
        this->boundary_conditions = boundary_conditions;
    }

    VectorXd solve(double resistivity) {
        assemble_global_matrices(resistivity);
        VectorXd potential = global_conductance.colPivHouseholderQr().solve(global_current);
        return potential;
    }

private:
    double x_dim;
    double y_dim;
    double z_dim;
    int x_ele;
    int y_ele;
    int z_ele;
    int num_nodes;
    int num_elements;
    vector<vector<int>> connectivity;
    vector<Vector3d> nodes;
    MatrixXd global_conductance;
    VectorXd global_current;
    vector<BoundaryCondition> boundary_conditions;

    vector<vector<int>> computeConnectivity() {
        vector<vector<int>> conn;
        int id = 0;
        for (int k = 0; k < z_ele; k++) {
            for (int j = 0; j < y_ele; j++) {
                for (int i = 0; i < x_ele; i++) {
                    vector<int> element_nodes = { id, id + 1, id + x_ele + 1, id + x_ele + 2,
                                                 id + (x_ele + 1) * (y_ele + 1),
                                                 id + (x_ele + 1) * (y_ele + 1) + 1,
                                                 id + (x_ele + 1) * (y_ele + 1) + x_ele + 1,
                                                 id + (x_ele + 1) * (y_ele + 1) + x_ele + 2 };
                    conn.push_back(element_nodes);
                    id++;
                }
                id++;
            }
            id += x_ele + 1;
        }
        return conn;
    }

    vector<Vector3d> computeNodes() {
        vector<Vector3d> nodes;
        double dx = x_dim / x_ele;
        double dy = y_dim / y_ele;
        double dz = z_dim / z_ele;
        for (int k = 0; k <= z_ele; k++) {
            for (int j = 0; j <= y_ele; j++) {
                for (int i = 0; i <= x_ele; i++) {
                    Vector3d node(i * dx, j * dy, k * dz);
                    nodes.push_back(node);
                }
            }
        }
        return nodes;
    }

    Matrix3d compute_element_conductance(double resistivity) {
        Matrix3d conductivity_matrix;
        conductivity_matrix << resistivity, 0, 0, 0, resistivity, 0, 0, 0, resistivity;
        Matrix3d element_conductance = conductivity_matrix.inverse();
        return element_conductance;
    }

    Matrix3d compute_jacobian(int element) {
        Vector3d P1 = nodes[connectivity[element][0]];
        Vector3d P2 = nodes[connectivity[element][1]];
        Vector3d P3 = nodes[connectivity[element][2]];
        Vector3d P4 = nodes[connectivity[element][3]];
        Vector3d P5 = nodes[connectivity[element][4]];
        Vector3d P6 = nodes[connectivity[element][5]];
        Vector3d P7 = nodes[connectivity[element][6]];
        Vector3d P8 = nodes[connectivity[element][7]];

        Matrix3d jacobian;
        jacobian << P2[0] - P1[0], P3[0] - P1[0], P5[0] - P1[0],
            P2[1] - P1[1], P3[1] - P1[1], P5[1] - P1[1],
            P2[2] - P1[2], P3[2] - P1[2], P5[2] - P1[2];

        return jacobian;
    }

    Matrix<double, 8, 3> compute_B_matrix(int element) {
        Matrix3d jacobian = compute_jacobian(element);
        Matrix3d jacobian_inv = jacobian.inverse();

        Matrix<double, 8, 3> B_matrix;
        B_matrix << jacobian_inv(0, 0), 0, 0,
            jacobian_inv(1, 0), 0, 0,
            jacobian_inv(2, 0), 0, 0,
            0, jacobian_inv(0, 1), 0,
            0, jacobian_inv(1, 1), 0,
            0, jacobian_inv(2, 1), 0,
            0, 0, jacobian_inv(0, 2),
            0, 0, jacobian_inv(1, 2),
            0, 0, jacobian_inv(2, 2);

                return B_matrix;
            }

            void assemble_global_matrices(double resistivity) {
                global_conductance = MatrixXd::Zero(num_nodes, num_nodes);
                global_current = VectorXd::Zero(num_nodes);

                Matrix3d element_conductance = compute_element_conductance(resistivity);

                for (int i = 0; i < num_elements; i++) {
                    Matrix<double, 8, 3> B_matrix = compute_B_matrix(i);
                    Matrix<double, 8, 8> element_conductance_matrix = B_matrix.transpose() * element_conductance * B_matrix;

                    for (int j = 0; j < 8; j++) {
                        int node1 = connectivity[i][j];
                        for (int k = 0; k < 8; k++) {
                            int node2 = connectivity[i][k];
                            global_conductance.coeffRef(node1, node2) += element_conductance_matrix(j, k);
                        }
                    }
                }

                for (auto bc : boundary_conditions) {
                    if (bc.type == 0) {
                        global_current[bc.node] = bc.value;
                        global_conductance.row(bc.node) = VectorXd::Zero(num_nodes);
                        global_conductance.col(bc.node) = VectorXd::Zero(num_nodes);
                        global_conductance.coeffRef(bc.node, bc.node) = 1;
                    }
                    else {
                        global_current[bc.node] += bc.value;
                    }
                }
            }
        };
*/

