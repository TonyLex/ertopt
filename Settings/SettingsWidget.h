#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QSpacerItem>

class SettingsWidget: public QWidget
{
public:
    SettingsWidget();

private:
    QComboBox * CBLanguage;

};

#endif // SETTINGSWIDGET_H
