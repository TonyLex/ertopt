#include "SettingsWidget.h"

SettingsWidget::SettingsWidget()
{
    CBLanguage = new QComboBox();
    CBLanguage->addItem(tr("English"));
    CBLanguage->addItem(tr("Russian"));

    QHBoxLayout *mainHLayout = new QHBoxLayout(this);

    QGroupBox * GBCommonSettings = new QGroupBox();
    QVBoxLayout *commonSettingVLayout = new QVBoxLayout(GBCommonSettings);

    QHBoxLayout *languageLayout = new QHBoxLayout(this);
    languageLayout->addWidget(new QLabel(tr("Language")));
    languageLayout->addWidget(CBLanguage);

    commonSettingVLayout->addLayout(languageLayout);
    commonSettingVLayout->addItem(new QSpacerItem(0,1000,QSizePolicy::Fixed, QSizePolicy::Expanding));

    mainHLayout->addWidget(GBCommonSettings);
    mainHLayout->addItem(new QSpacerItem(1000,0,QSizePolicy::Expanding, QSizePolicy::Fixed));

}
