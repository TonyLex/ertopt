#ifndef GRAPHEDITORWIDGET_H
#define GRAPHEDITORWIDGET_H

#include <QWidget>

#include <Qt3DRender/qcamera.h>
#include <Qt3DCore/qentity.h>
#include <Qt3DRender/qcameralens.h>

#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtGui/QScreen>

#include <Qt3DExtras/qtorusmesh.h>
#include <Qt3DRender/qmesh.h>
#include <Qt3DRender/qtechnique.h>
#include <Qt3DRender/qmaterial.h>
#include <Qt3DRender/qeffect.h>
#include <Qt3DRender/qtexture.h>
#include <Qt3DRender/qrenderpass.h>
#include <Qt3DRender/qsceneloader.h>
#include <Qt3DRender/qpointlight.h>

#include <Qt3DCore/qtransform.h>
#include <Qt3DCore/qaspectengine.h>

#include <Qt3DRender/qrenderaspect.h>
#include <Qt3DExtras/qforwardrenderer.h>

#include <Qt3DExtras/qt3dwindow.h>
#include <Qt3DExtras/qfirstpersoncameracontroller.h>
#include <Qt3DExtras/QOrbitCameraController>

#include <GraphicEditor3D/scenemodifier.h>

#include <GraphicEditor3D/Window3DCustom.h>

#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include "GraphicEditor3D/Shapes/ShapeManager.h"

#include <CommonGui/CommonButton.h>
#include <Grid/Node.h>
#include <Files/SaveToPolyFile.h>

class GraphEditorWidget: public QWidget
{
    Q_OBJECT

public:
    GraphEditorWidget(QWidget *parent = nullptr);
    ~GraphEditorWidget();

    QLineEdit * LEShape_X;
    QLineEdit * LEShape_Y;
    QLineEdit * LEShape_Z;

    QLineEdit * LEShape_WidthX;
    QLineEdit * LEShape_WidthY;
    QLineEdit * LEShape_WidthZ;

    QLineEdit * LHalfSpace_X_min;
    QLineEdit * LHalfSpace_Y_min;
    QLineEdit * LHalfSpace_Z_min;
    QLineEdit * LHalfSpace_X_max;
    QLineEdit * LHalfSpace_Y_max;
    QLineEdit * LHalfSpace_Z_max;
    QLineEdit * LHalfSpace_Cond;

    QLineEdit * LEShape_Cond;

signals:
    void signal_setShapeParameters(QVector3D pos, QVector3D width, qreal cond);

public slots:
    void newShapeCreated(Shape * shape);
    void setShapeParameters(QVector3D pos, QVector3D width, qreal cond);
    void ParamChangedInLineEdit(const QString &value);
    void createGrid();

    void bindSize();

protected:
    void keyPressEvent(QKeyEvent *event) override;


private:
    Window3DCustom *view;
    SceneModifier *modifier;
    Qt3DRender::QCamera *cameraEntity;
    Qt3DExtras::QOrbitCameraController *camController;
    Grid *grid;

    CommonButton *bindSizeButton;

    bool bindPositionOk = false;
    bool bindSizeOk = false;

    SaveToPolyFile *save_to_file;


    QHBoxLayout *getHLayout(QLineEdit *LE, QString title);

};

#endif // GRAPHEDITORWIDGET_H
