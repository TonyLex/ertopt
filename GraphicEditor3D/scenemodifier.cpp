#include "scenemodifier.h"
#include <Qt3DRender/QGeometryRenderer>
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QAttribute>
#include <QtCore/QDebug>
//#include <Qt3DRender/Qt>
#include <Qt3DExtras/QForwardRenderer>

#include <Qt3DExtras/QText2DEntity>
#include <Qt3DExtras/QExtrudedTextMesh>

#include <Qt3DInput/QAxis>


SceneModifier::SceneModifier(Qt3DCore::QEntity *rootEntity, Qt3DRender::QCamera *cameraEntity_l)
    : m_rootEntity(rootEntity)
{
    cameraEntity = cameraEntity_l;
    timer_rotate = new QTimer();
    connect(timer_rotate, &QTimer::timeout, this, &SceneModifier::rotateTorus);

    picker = new Qt3DRender::QObjectPicker(m_rootEntity);

    createAxis(20, 1);


    //Qt3DRender::QFrameGraph *frameGraph = new Qt3DRender::QFrameGraph();
    //Qt3DRender::QForwardRenderer *forwardRenderer = new Qt3DRender::QForwardRenderer();


    //auto objectPicker = new Qt3DRender::QObjectPicker(this);

    //QObject::connect(objectPicker, SIGNAL(clicked(Qt3DRender::QPickEvent*)), this, SLOT(onClicked(Qt3DRender::QPickEvent*)));
    //sphere_1->setPicker(picker);

    //cylinder_1->setPicker(picker);



    timer_rotate->start(100);

    half_space = new HalfSpace(m_rootEntity);

    //setHalfSpaceParameters(-10,10,-10,10,0,10, 5);

//--------- 3D Surface Start ----------------------------------------

    // ConeMesh Transform
    Qt3DCore::QTransform *surfTransform = new Qt3DCore::QTransform();
    surfTransform->setScale(1.0f);
    //surfTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 0.0f));
    //coneTransform->setTranslation(QVector3D(0.0f, 4.0f, -1.5));

    Qt3DExtras::QPhongAlphaMaterial *surfMaterial = new Qt3DExtras::QPhongAlphaMaterial();
    surfMaterial->setDiffuse(Qt::green);
    surfMaterial->setAlpha(0.5);

    // Create a surface entity
    Qt3DCore::QEntity *surfEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create a geometry
        Qt3DRender::QGeometry *geometry = new Qt3DRender::QGeometry();

        // Create a buffer for vertex positions
        Qt3DRender::QBuffer *vertexBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);

        QVector<float> vertices;
        int gridSize = 10;
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                // Add the vertex coordinates to the vector
                vertices.append(i);
                vertices.append(j);
                vertices.append(i);
            }
        }
        vertexBuffer->setData(QByteArray((const char*)vertices.data(), vertices.size() * sizeof(float)));

        // Create a vertex attribute for positions
        Qt3DRender::QAttribute *positionAttribute = new Qt3DRender::QAttribute(geometry);
        positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
        positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
        positionAttribute->setVertexSize(3);
        positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        positionAttribute->setBuffer(vertexBuffer);
        positionAttribute->setByteStride(3 * sizeof(float));
        positionAttribute->setCount(3); // number of vertices
        geometry->addAttribute(positionAttribute);

        // Create a geometry renderer
        Qt3DRender::QGeometryRenderer *geometryRenderer = new Qt3DRender::QGeometryRenderer();
        geometryRenderer->setGeometry(geometry);

        surfEntity->addComponent(geometryRenderer);
        surfEntity->addComponent(surfMaterial);
        surfEntity->addComponent(surfTransform);

//--------- 3D Surface End ----------------------------------------
}

SceneModifier::~SceneModifier()
{
}

void SceneModifier::rotateTorus()
{
    angle_rotate_torus++;
    if(angle_rotate_torus == 360){
        angle_rotate_torus = 0;
    }
    //rootEntityTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 0.0f, 1.0f), angle_rotate_torus));
    //rootEntityTransform->setScale(0.01*angle_rotate_torus);


}

void SceneModifier::createSphere()
{
    Sphere *sphere;
    sphere = new Sphere(m_rootEntity);
    ShapeManager::getInstance()->AddShape(sphere);
    SelectionShape::getInstanse()->Select(sphere);
}

void SceneModifier::createCone()
{
    Cone *cone;
    cone = new Cone(m_rootEntity);
    ShapeManager::getInstance()->AddShape(cone);
    SelectionShape::getInstanse()->Select(cone);
}

void SceneModifier::createCuboid()
{
    Cuboid *shape;
    shape = new Cuboid(m_rootEntity);
    ShapeManager::getInstance()->AddShape(shape);
    SelectionShape::getInstanse()->Select(shape);
}

void SceneModifier::createTorus()
{
    Torum *shape;
    shape = new Torum(m_rootEntity);
    ShapeManager::getInstance()->AddShape(shape);
    SelectionShape::getInstanse()->Select(shape);
}

void SceneModifier::createPlane()
{
    Plane *shape;
    shape = new Plane(m_rootEntity);
    ShapeManager::getInstance()->AddShape(shape);
    SelectionShape::getInstanse()->Select(shape);
}

void SceneModifier::createCylinder()
{
    Cylinder *shape;
    shape = new Cylinder(m_rootEntity);
    ShapeManager::getInstance()->AddShape(shape);
    SelectionShape::getInstanse()->Select(shape);
}

void SceneModifier::setHalfSpaceParameters(qreal xMin, qreal xMax, qreal yMin, qreal yMax, qreal zMin, qreal zMax, qreal cond)
{
    //half_space->setScale(QVector3D(dx, dy, dz));
    //half_space->setPosition(QVector3D(0, 0, dz/2));
    half_space->setParam(xMin, xMax, yMin, yMax, zMin, zMax);
    half_space->setCond(cond);
}

void SceneModifier::drawGrid(Grid &grid) const
{
    qDebug()<<"SceneModifier::drawGrid" << grid.getNodeList().size();
    qreal condMax = 100;
    qreal condMin = 0;

    float scale = 0.3;
/*
    for(auto &node:grid.getNodeList()){

        if(node->cond!=1){
            qDebug()<<"SceneModifier::drawGrid "<<node->coord;
            Sphere *sphere;
            sphere = new Sphere(m_rootEntity);
            qreal condPixel = 255*node->cond/(condMax-condMin);

            sphere->setScale({scale,scale,scale});

            sphere->getMaterial()->setDiffuse(QColor(Qt::red));//condPixel,condPixel,condPixel));

            sphere->getMaterial()->setAlpha(0.7);
            sphere->setPosition(node->coord);

        }

    }
    */

}

void SceneModifier::setCamera(Qt3DRender::QCamera *cameraEntity_l)
{
    cameraEntity = cameraEntity_l;
}

void SceneModifier::drawLine(const QVector3D &start, const QVector3D &end, const QColor &color, Qt3DCore::QEntity *_rootEntity)
{
    auto *geometry = new Qt3DRender::QGeometry(_rootEntity);

    // position vertices (start and end)
    QByteArray bufferBytes;
    bufferBytes.resize(3 * 2 * sizeof(float)); // start.x, start.y, start.end + end.x, end.y, end.z
    float *positions = reinterpret_cast<float*>(bufferBytes.data());
    *positions++ = start.x();
    *positions++ = start.y();
    *positions++ = start.z();
    *positions++ = end.x();
    *positions++ = end.y();
    *positions++ = end.z();

    auto *buf = new Qt3DRender::QBuffer(geometry);
    buf->setData(bufferBytes);

    auto *positionAttribute = new Qt3DRender::QAttribute(geometry);
    positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
    positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    positionAttribute->setVertexSize(3);
    positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    positionAttribute->setBuffer(buf);
    positionAttribute->setByteStride(3 * sizeof(float));
    positionAttribute->setCount(2);
    geometry->addAttribute(positionAttribute); // We add the vertices in the geometry

    // connectivity between vertices
    QByteArray indexBytes;
    indexBytes.resize(2 * sizeof(unsigned int)); // start to end
    unsigned int *indices = reinterpret_cast<unsigned int*>(indexBytes.data());
    *indices++ = 0;
    *indices++ = 1;

    auto *indexBuffer = new Qt3DRender::QBuffer(geometry);
    indexBuffer->setData(indexBytes);

    auto *indexAttribute = new Qt3DRender::QAttribute(geometry);
    indexAttribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    indexAttribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    indexAttribute->setBuffer(indexBuffer);
    indexAttribute->setCount(2);
    geometry->addAttribute(indexAttribute); // We add the indices linking the points in the geometry

    // mesh
    auto *line = new Qt3DRender::QGeometryRenderer(_rootEntity);
    line->setGeometry(geometry);
    line->setPrimitiveType(Qt3DRender::QGeometryRenderer::Lines);
    auto *material = new Qt3DExtras::QPhongMaterial(_rootEntity);
    material->setAmbient(color);

    // entity
    auto *lineEntity = new Qt3DCore::QEntity(_rootEntity);
    lineEntity->addComponent(line);
    lineEntity->addComponent(material);
}

void SceneModifier::createAxis(qreal Max_value, qreal step)
{
    float axis_line_length = Max_value;
    // Добавление оси X
    QVector3D x_axis_start = {-(axis_line_length),0,0};
    QVector3D x_axis_end = {(axis_line_length),0,0};
    drawLine(x_axis_start, x_axis_end, Qt::red, m_rootEntity);

    // Добавление оси Y
    QVector3D y_axis_start = {0,-(axis_line_length),0};
    QVector3D y_axis_end = {0,(axis_line_length),0};
    drawLine(y_axis_start, y_axis_end, Qt::green, m_rootEntity);

    // Добавление оси Z
    QVector3D z_axis_start = {0,0,-(axis_line_length)};
    QVector3D z_axis_end = {0,0,(axis_line_length)};
    drawLine(z_axis_start, z_axis_end, Qt::blue, m_rootEntity);

    qreal height = 20;
    qreal weight = 100;
    QFont font("monospace");
    qreal scale = 0.02f;

    for(qreal i = -Max_value; i<=Max_value; i+=step){

        auto *text2dTransformX = new Qt3DCore::QTransform;
        text2dTransformX->setScale(scale);
        text2dTransformX->setTranslation(QVector3D(i, 0, 0));
        text2dTransformX->setRotation(cameraEntity->transform()->rotation());

        auto *text2dTransformY = new Qt3DCore::QTransform;
        text2dTransformY->setScale(scale);
        text2dTransformY->setTranslation(QVector3D(0, i, 0));
        text2dTransformY->setRotation(cameraEntity->transform()->rotation());

        auto *text2dTransformZ = new Qt3DCore::QTransform;
        text2dTransformZ->setScale(scale);
        text2dTransformZ->setTranslation(QVector3D(0, 0, i));
        text2dTransformZ->setRotation(cameraEntity->transform()->rotation());

        auto *text2dX = new Qt3DExtras::QText2DEntity(m_rootEntity);
        text2dX->setFont(font);
        text2dX->setHeight(height);
        text2dX->setWidth(weight);
        text2dX->setText(QString::number(i));
        text2dX->setColor(Qt::black);
        text2dX->addComponent(text2dTransformX);

        auto *text2dY = new Qt3DExtras::QText2DEntity(m_rootEntity);
        text2dY->setFont(font);
        text2dY->setHeight(height);
        text2dY->setWidth(weight);
        text2dY->setText(QString::number(i));
        text2dY->setColor(Qt::black);
        text2dY->addComponent(text2dTransformY);

        auto *text2dZ = new Qt3DExtras::QText2DEntity(m_rootEntity);
        text2dZ->setFont(font);
        text2dZ->setHeight(height);
        text2dZ->setWidth(weight);
        text2dZ->setText(QString::number(i));
        text2dZ->setColor(Qt::black);
        text2dZ->addComponent(text2dTransformZ);

    }

    // Добавляю подписи К осям:

    font.bold();
    scale *=2;

    auto *Titletext2dTransformX = new Qt3DCore::QTransform;
    Titletext2dTransformX->setScale(scale);
    Titletext2dTransformX->setTranslation(QVector3D(Max_value, 0, 0));
    Titletext2dTransformX->setRotation(cameraEntity->transform()->rotation());

    auto *Titletext2dTransformY = new Qt3DCore::QTransform;
    Titletext2dTransformY->setScale(scale);
    Titletext2dTransformY->setTranslation(QVector3D(0, Max_value, 0));
    Titletext2dTransformY->setRotation(cameraEntity->transform()->rotation());

    auto *Titletext2dTransformZ = new Qt3DCore::QTransform;
    Titletext2dTransformZ->setScale(scale);
    Titletext2dTransformZ->setTranslation(QVector3D(0, 0, Max_value));
    Titletext2dTransformZ->setRotation(cameraEntity->transform()->rotation());

    auto *text2dX = new Qt3DExtras::QText2DEntity(m_rootEntity);
    text2dX->setFont(font);
    text2dX->setHeight(height);
    text2dX->setWidth(weight);
    text2dX->setText("X");
    text2dX->setColor(Qt::black);
    text2dX->addComponent(Titletext2dTransformX);

    auto *text2dY = new Qt3DExtras::QText2DEntity(m_rootEntity);
    text2dY->setFont(font);
    text2dY->setHeight(height);
    text2dY->setWidth(weight);
    text2dY->setText("Y");
    text2dY->setColor(Qt::black);
    text2dY->addComponent(Titletext2dTransformY);

    auto *text2dZ = new Qt3DExtras::QText2DEntity(m_rootEntity);
    text2dZ->setFont(font);
    text2dZ->setHeight(height);
    text2dZ->setWidth(weight);
    text2dZ->setText("Z");
    text2dZ->setColor(Qt::black);
    text2dZ->addComponent(Titletext2dTransformZ);

}
