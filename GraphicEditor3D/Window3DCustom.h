#ifndef WINDOW3DCUSTOM_H
#define WINDOW3DCUSTOM_H

#include <Qt3DExtras/qt3dwindow.h>
#include <Qt3DCore>
#include <QMouseEvent>
#include <Qt3DExtras/qfirstpersoncameracontroller.h>

class Window3DCustom: public Qt3DExtras::Qt3DWindow
{
    //Q_OBJECT
public:
    Window3DCustom(Qt3DCore::QEntity * scene);
    QVector3D mouseEventToSpace(const QPointF pos, const Qt3DRender::QCamera* camera, const QSurface* surface);

protected:
    /*void mousePressEvent(QMouseEvent *eventPress) override;
    void mouseMoveEvent(QMouseEvent *eventMove) override;
    void mouseReleaseEvent(QMouseEvent *eventRelease) override;
    void wheelEvent(QWheelEvent *event) override;*/


private:
    // Root entity
    Qt3DCore::QEntity *scene;

    qreal angle_rotate_scene;

    QPointF startPointF;
    QPointF endPointF;

    bool leftButtonIsPressed = false;

    Qt3DCore::QTransform *rootEntityTransform;
};

#endif // WINDOW3DCUSTOM_H
