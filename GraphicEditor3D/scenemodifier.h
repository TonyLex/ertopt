/****************************************************************************
**
** Copyright (C) 2014 Klaralvdalens Datakonsult AB (KDAB).
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt3D module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SCENEMODIFIER_H
#define SCENEMODIFIER_H

#include <QtCore/QObject>

#include <Qt3DCore/qentity.h>
#include <Qt3DCore/qtransform.h>

#include <Qt3DExtras/QTorusMesh>
#include <Qt3DExtras/QConeMesh>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QPlaneMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QPhongMaterial>

#include <GraphicEditor3D/Shapes/Sphere.h>
#include <GraphicEditor3D/Shapes/Cuboid.h>
#include <GraphicEditor3D/Shapes/Cylinder.h>
#include <GraphicEditor3D/Shapes/Cone.h>
#include <GraphicEditor3D/Shapes/Torum.h>
#include <GraphicEditor3D/Shapes/Plane.h>

#include <GraphicEditor3D/Shapes/HalfSpace.h>

#include <Qt3DRender/QObjectPicker>
#include <GraphicEditor3D/Shapes/ShapeManager.h>

#include <Grid/Grid.h>

#include <QTimer>

class SceneModifier : public QObject
{
    Q_OBJECT

public:
    explicit SceneModifier(Qt3DCore::QEntity *rootEntity, Qt3DRender::QCamera *cameraEntity_l);
    ~SceneModifier();
    void drawGrid(Grid &grid) const;
    void setCamera(Qt3DRender::QCamera *cameraEntity);

public slots:
    void rotateTorus();
    void createSphere();
    void createCone();
    void createCuboid();
    void createTorus();
    void createPlane();
    void createCylinder();

    void setHalfSpaceParameters(qreal xMin, qreal xMax, qreal yMin, qreal yMax, qreal zMin, qreal zMax, qreal cond);



private:
    Qt3DCore::QEntity *m_rootEntity;
    Qt3DCore::QEntity *m_planeEntity;
    Qt3DRender::QCamera *cameraEntity;

    Qt3DCore::QTransform *torusTransform;
    qreal angle_rotate_torus = 0;

    void drawLine(const QVector3D& start, const QVector3D& end, const QColor& color, Qt3DCore::QEntity *_rootEntity);

    Qt3DRender::QObjectPicker *picker;

    QTimer *timer_rotate;

    void createAxis(qreal Max_value, qreal step);

    HalfSpace *half_space;

};

#endif // SCENEMODIFIER_H

