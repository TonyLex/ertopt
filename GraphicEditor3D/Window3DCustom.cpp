#include "Window3DCustom.h"
#include <QDebug>
#include <QMatrix4x4>
#include <Qt3DRender/QCamera>

Window3DCustom::Window3DCustom(Qt3DCore::QEntity * scene_l)
    :scene(scene_l)
{
    //this->setMouseGrabEnabled(false);

    rootEntityTransform = new Qt3DCore::QTransform();
    rootEntityTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 0.0f, 1.0f), 25.0f));
    scene -> addComponent(rootEntityTransform);


    /*
    angle_rotate_torus++;
    if(angle_rotate_torus == 360){
        angle_rotate_torus = 0;
    }*/
    //rootEntityTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 0.0f, 1.0f), angle_rotate_torus));
    //rootEntityTransform->setScale(0.01*angle_rotate_torus);

}
/*
void Window3DCustom::mousePressEvent(QMouseEvent *eventPress)
{
    qDebug()<<"mousePressEvent";
    if(eventPress->button() == Qt::LeftButton) {
        leftButtonIsPressed = true;
        startPointF = eventPress->pos();
    }
}

void Window3DCustom::mouseMoveEvent(QMouseEvent *event)
{
    qDebug()<<"mouseMoveEvent";
    endPointF = event->pos();
    qreal speed_rotation = 1;
    if(leftButtonIsPressed){
        QVector2D vectStartMinEnd(startPointF - endPointF);
        if(qAbs(vectStartMinEnd.x())>qAbs(vectStartMinEnd.y())){
            angle_rotate_scene = -vectStartMinEnd.x()*speed_rotation;
            rootEntityTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 0.0f, 1.0f), angle_rotate_scene));
        } else{
            angle_rotate_scene = -vectStartMinEnd.y()*speed_rotation;
            rootEntityTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 1.0f, 0.0f), angle_rotate_scene));
        }

    }
}

void Window3DCustom::mouseReleaseEvent(QMouseEvent *eventRelease)
{
    qDebug()<<"QMouseRealizeEvent";
    if(eventRelease->button() == Qt::LeftButton) {
        leftButtonIsPressed = false;
    }
}

void Window3DCustom::wheelEvent(QWheelEvent *event)
{
    QPoint numDegrees = event->angleDelta() / 8;

    qreal speed = 0.05;
        if (numDegrees.y() > 0)
        {
            qDebug()<<"wheelUp";
            //mouseEventToSpace(event->position(),camera(), )
            //rootEntityTransform->setTranslation(QVector3D(event->position()));
            rootEntityTransform->setScale(rootEntityTransform->scale()/(numDegrees.y()*speed));

        }
        else
        {
            qDebug()<<"wheelDown";
            //rootEntityTransform->setTranslation(QVector3D(event->position()));
            rootEntityTransform->setScale(rootEntityTransform->scale()*(qAbs(numDegrees.y())*speed));
        }
}
*/
QVector3D Window3DCustom::mouseEventToSpace(const QPointF pos, const Qt3DRender::QCamera* camera, const QSurface* surface) {

const QPointF glCorrectSurfacePosition{static_cast<float>(pos.x()),
                                       surface->size().height() - static_cast<float>(pos.y())};
const QMatrix4x4 viewMatrix = camera->viewMatrix();
const QMatrix4x4 projectionMatrix{camera->lens()->projectionMatrix()};

const int areaWidth         = surface->size().width();
const int areaHeight        = surface->size().height();
const auto relativeViewport = QRectF(0.0f, 0.0f, 1.0f, 1.0f);
const auto viewport =
    QRect(relativeViewport.x() * areaWidth, (1.0 - relativeViewport.y() - relativeViewport.height()) * areaHeight,
          relativeViewport.width() * areaWidth, relativeViewport.height() * areaHeight);
const auto nearPos = QVector3D{static_cast<float>(glCorrectSurfacePosition.x()),
                               static_cast<float>(glCorrectSurfacePosition.y()), 0.0f}
                         .unproject(viewMatrix, projectionMatrix, viewport);
return nearPos;

}
