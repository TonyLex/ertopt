#include "GraphicEditor3D/GraphEditorWidget.h"

GraphEditorWidget::GraphEditorWidget(QWidget *parent)
    : QWidget(parent)
{
    connect(ShapeManager::getInstance(), &ShapeManager::signal_NewShape, this, &GraphEditorWidget::newShapeCreated);

    bindSizeButton = new CommonButton(":/Icons/Bind.png", tr("Bind size change"));

    bindSizeButton->makeGrayBackground();

    save_to_file = new SaveToPolyFile(parent);

    grid = new Grid();
    // Root entity
    Qt3DCore::QEntity *sceneEntity = new Qt3DCore::QEntity();

    view = new Window3DCustom(sceneEntity);
    view->defaultFrameGraph()->setClearColor(Qt::white);
    QWidget *container = QWidget::createWindowContainer(view);
    container->setMouseTracking(true);
    QSize screenSize = view->screen()->size();
    container->setMinimumSize(QSize(200, 100));
    container->setMaximumSize(screenSize);

    //QWidget *widget = new QWidget;
    QHBoxLayout *hLayout = new QHBoxLayout(this);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->setAlignment(Qt::AlignTop);
    hLayout->addLayout(vLayout);
    hLayout->addWidget(container, 1);


    this->setWindowTitle(QStringLiteral("ErtOpt"));

    // Camera
    cameraEntity = view->camera();

    //view->camera()->lens()->setPerspectiveProjection(45.0f, view->width()/view->height(), 0.01f, 5000.0f);

    cameraEntity->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    cameraEntity->setPosition(QVector3D(-20,20, -20.0f));
    cameraEntity->setUpVector(QVector3D(0, 0, -1));
    cameraEntity->setViewCenter(QVector3D(0, 0, 0));

    Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(cameraEntity);
    Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(1);
    lightEntity->addComponent(light);
    Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform();
    lightTransform->setTranslation({100,100,100});
    //lightTransform->setTranslation(cameraEntity->position());
    lightEntity->addComponent(lightTransform);

    // For camera controls
    camController = new Qt3DExtras::QOrbitCameraController(cameraEntity);

    //Qt3DExtras::QFirstPersonCameraController *camController = new Qt3DExtras::QFirstPersonCameraController(cameraEntity);
    int cameraSpeed = 100;
    camController->setCamera(cameraEntity);
    camController->setLinearSpeed(cameraSpeed);
    camController->setAcceleration(1);
    //camController->set

    // Scenemodifier
    modifier = new SceneModifier(sceneEntity, cameraEntity);

    // Set root object of the scene
    view->setRootEntity(sceneEntity);

    // Create control widgets
    CommonButton *PBCreateSphere = new CommonButton(":/Images/create sphere.png",tr("Create Sphere"));
    CommonButton *PBCreateCuboid = new CommonButton(":/Images/create cuboid.png",tr("Create Cuboid"));
    CommonButton *PBCreateCylinder = new CommonButton(":/Images/create cylinder.png",tr("Create Cylinder"));
    CommonButton *PBCreateTorus = new CommonButton(":/Images/create torum.jpg",tr("Create Torus"));
    CommonButton *PBCreateCone = new CommonButton(":/Images/create cone.jpg",tr("Create Cone"));

    CommonButton *PBSaveFileAs = new CommonButton(":/Icons/save.png",tr("Save figures in file"));

    //CommonButton *PBCreatePlane = new CommonButton("hhh",tr("Create Plane"));

    QGroupBox * GBToolBar = new QGroupBox(tr("ToolBar"));
    QHBoxLayout *hLauoutShapeButton = new QHBoxLayout(GBToolBar);

    hLauoutShapeButton->addWidget(PBCreateSphere);
    hLauoutShapeButton->addWidget(PBCreateCuboid);
    hLauoutShapeButton->addWidget(PBCreateCylinder);
    hLauoutShapeButton->addWidget(PBCreateTorus);
    hLauoutShapeButton->addWidget(PBCreateCone);

    hLauoutShapeButton->addWidget(PBSaveFileAs);

    //hLauoutShapeButton->addWidget(PBCreatePlane);

    vLayout->addWidget(GBToolBar);

    LEShape_X = new QLineEdit("0");
    LEShape_Y = new QLineEdit("0");
    LEShape_Z = new QLineEdit("0");

    LEShape_WidthX = new QLineEdit("0");
    LEShape_WidthY = new QLineEdit("0");
    LEShape_WidthZ = new QLineEdit("0");

    LEShape_Cond = new QLineEdit("0");

    QHBoxLayout *hLayout_param_x = getHLayout(LEShape_X,tr("X coordinate"));
    QHBoxLayout *hLayout_param_y = getHLayout(LEShape_Y,tr("Y coordinate"));
    QHBoxLayout *hLayout_param_z = getHLayout(LEShape_Z,tr("Z coordinate"));

    QHBoxLayout *hLayout_param_widthX = getHLayout(LEShape_WidthX,tr("X Scale"));
    QHBoxLayout *hLayout_param_widthY = getHLayout(LEShape_WidthY,tr("Y Scale"));
    QHBoxLayout *hLayout_param_widthZ = getHLayout(LEShape_WidthZ,tr("Z Scale"));

    QHBoxLayout *hLayout_param_cond = getHLayout(LEShape_Cond,tr("Conductivity"));


    QGroupBox * GBShapePositionParameters = new QGroupBox(tr("Shape Position"));
    QHBoxLayout *hLayout_position = new QHBoxLayout(GBShapePositionParameters);

    QVBoxLayout *vLayout_position_param = new QVBoxLayout();

    vLayout_position_param->addLayout(hLayout_param_x);
    vLayout_position_param->addLayout(hLayout_param_y);
    vLayout_position_param->addLayout(hLayout_param_z);

    hLayout_position->addLayout(vLayout_position_param);
    //hLayout_position->addWidget(bindPositionButton);

    QGroupBox * GBShapeSizeParameters = new QGroupBox(tr("Shape Size"));
    QHBoxLayout *hLayout_Size = new QHBoxLayout(GBShapeSizeParameters);

    QVBoxLayout *vLayout_size_param = new QVBoxLayout();

    vLayout_size_param->addLayout(hLayout_param_widthX);
    vLayout_size_param->addLayout(hLayout_param_widthY);
    vLayout_size_param->addLayout(hLayout_param_widthZ);

    hLayout_Size->addLayout(vLayout_size_param);
    hLayout_Size->addWidget(bindSizeButton);

    vLayout->addWidget(GBShapePositionParameters);
    vLayout->addWidget(GBShapeSizeParameters);

    vLayout->addLayout(hLayout_param_cond);

    QPushButton *PBCreateGrid = new QPushButton(tr("Create Grid"));
    vLayout->addWidget(PBCreateGrid);

    connect(PBCreateGrid, &QPushButton::clicked, this, &GraphEditorWidget::createGrid);

    QLabel * LCond_default = new QLabel(tr("Default conductivity"));
    QLineEdit *LECond_default = new QLineEdit("10");
    QHBoxLayout *hLayout_cond_default = new QHBoxLayout();
    hLayout_cond_default->addWidget(LCond_default);
    hLayout_cond_default->addWidget(LECond_default);

    vLayout->addLayout(hLayout_cond_default);

    connect(LEShape_X, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_Y, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_Z, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_WidthX, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_WidthY, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_WidthZ, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);
    connect(LEShape_Cond, &QLineEdit::textEdited, this, &GraphEditorWidget::ParamChangedInLineEdit);

    connect(PBCreateSphere, &CommonButton::clicked, modifier, &SceneModifier::createSphere);
    connect(PBCreateCuboid, &CommonButton::clicked, modifier, &SceneModifier::createCuboid);
    connect(PBCreateCylinder, &CommonButton::clicked, modifier, &SceneModifier::createCylinder);
    connect(PBCreateTorus, &CommonButton::clicked, modifier, &SceneModifier::createTorus);
    connect(PBCreateCone, &CommonButton::clicked, modifier, &SceneModifier::createCone);
    //connect(PBCreatePlane, &QPushButton::clicked, modifier, &SceneModifier::createPlane);

    connect(PBSaveFileAs, &CommonButton::clicked, save_to_file, &SaveToPolyFile::SaveFileAs);


    connect(bindSizeButton, &CommonButton::clicked, this, &GraphEditorWidget::bindSize);
}

GraphEditorWidget::~GraphEditorWidget()
{

}

void GraphEditorWidget::newShapeCreated(Shape *shape)
{
    qDebug()<<"Widget::newShapeCreated";
    connect(shape, &Shape::signal_Parameters, this, &GraphEditorWidget::setShapeParameters);
}

void GraphEditorWidget::setShapeParameters(QVector3D pos, QVector3D width, qreal cond)
{

    QList<Shape *> shape_list_all = ShapeManager::getInstance()->getShapesList();

    for(auto &shape:shape_list_all){
       disconnect(this, &GraphEditorWidget::signal_setShapeParameters, shape, &Shape::setParams);
    }

    QList<Shape *> shape_list = SelectionShape::getInstanse()->getSelectionShapesList();

    if(shape_list.size()==1){
        connect(this, &GraphEditorWidget::signal_setShapeParameters, shape_list.at(0), &Shape::setParams);
    }

    qDebug()<<"pos = "<< pos <<"width = "<< width << "cond" <<cond;
    LEShape_X->setText(QString::number(pos.x()));
    LEShape_Y->setText(QString::number(pos.y()));
    LEShape_Z->setText(QString::number(pos.z()));

    LEShape_WidthX->setText(QString::number(width.x()));
    LEShape_WidthY->setText(QString::number(width.y()));
    LEShape_WidthZ->setText(QString::number(width.z()));

    LEShape_Cond->setText(QString::number(cond));
}

void GraphEditorWidget::ParamChangedInLineEdit(const QString &value)
{
    qDebug()<< "value = "<< value<< " LEShape_X = "<< LEShape_X->text();
    //qDebug()<< "Widget::ParamChangedInLineEdit";
    QVector3D pos;
    QVector3D width;

    if(bindSizeOk){
        if(sender()==LEShape_WidthX || sender()==LEShape_WidthY || sender()==LEShape_WidthZ){
            LEShape_WidthX->setText(value);
            LEShape_WidthY->setText(value);
            LEShape_WidthZ->setText(value);
        }
    }

    pos.setX(LEShape_X->text().toDouble());
    pos.setY(LEShape_Y->text().toDouble());
    pos.setZ(LEShape_Z->text().toDouble());

    width.setX(LEShape_WidthX->text().toDouble());
    width.setY(LEShape_WidthY->text().toDouble());
    width.setZ(LEShape_WidthZ->text().toDouble());

    qreal cond = LEShape_Cond->text().toDouble();

    emit signal_setShapeParameters(pos,width,cond);
}

void GraphEditorWidget::createGrid()
{
  qDebug()<<"Widget::createGrid()";
  Grid grid;
  LimitsAndSteps lim;
  grid.setLimits(lim);
  grid.createGrid();

  modifier->drawGrid(grid);
  //LimitsAndSteps lims;
  //Node *node = new Node({0,0,0}, lims);
}

void GraphEditorWidget::bindSize()
{
    if(bindSizeOk){
        bindSizeOk = false;
        bindSizeButton->makeGrayBackground();
    }
    else{
        bindSizeOk = true;
        bindSizeButton->makeDarkBackground();
    }
}

void GraphEditorWidget::keyPressEvent(QKeyEvent *event)
{
    qDebug()<< event->key();
    if(event->key()==Qt::Key_Delete
            && SelectionShape::getInstanse()->getSelectionShapesList().size()==1){
        qDebug()<<"Delete Shape";
        Shape *shape = SelectionShape::getInstanse()->getSelectionShapesList().at(0);
        ShapeManager::getInstance()->DeleteShape(shape);
    }
    QWidget::keyPressEvent(event);
}

QHBoxLayout *GraphEditorWidget::getHLayout(QLineEdit *LE, QString title)
{
    QHBoxLayout *hLayout = new QHBoxLayout();
    QLabel * LShape_X = new QLabel(tr("X coordinate"));
    hLayout->addWidget(LShape_X);
    hLayout->addWidget(LE);

    return hLayout;
}

