#ifndef SPHERE_H
#define SPHERE_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Sphere: public Shape
{
    Q_OBJECT
public:
    Sphere(Qt3DCore::QEntity *rootEntity);

    bool intersectPoint(QVector3D &point) override;

public slots:
    //void handlePickerClicked(Qt3DRender::QPickEvent *pick) override;

};

#endif // SPHERE_H
