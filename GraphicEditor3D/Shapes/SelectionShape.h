#ifndef SELECTIONSHAPE_H
#define SELECTIONSHAPE_H

#include <GraphicEditor3D/Shapes/Shape.h>

class SelectionShape : public Shape
{
     Q_OBJECT
public:
    SelectionShape(Qt3DCore::QEntity *rootEntity = nullptr);
    static SelectionShape *getInstanse();

    void Select(Shape *shape);
    void DeSelect(Shape *shape);
    void DeselectAll();

    QList<Shape *> getSelectionShapesList();

signals:
    void signal_selectionChange();

private:
    static SelectionShape* self;
    QList<Shape *> SelectionShapesList;
};

#endif // SELECTIONSHAPE_H
