#ifndef Plane_H
#define Plane_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Plane: public Shape
{
     Q_OBJECT
public:
    Plane(Qt3DCore::QEntity *rootEntity);
};

#endif // Plane_H
