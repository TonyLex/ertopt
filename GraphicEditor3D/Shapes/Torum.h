#ifndef Torum_H
#define Torum_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Torum: public Shape
{
     Q_OBJECT
public:
    Torum(Qt3DCore::QEntity *rootEntity);
};

#endif // Torum_H
