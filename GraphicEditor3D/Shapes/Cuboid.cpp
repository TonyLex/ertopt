#include "Cuboid.h"

Cuboid::Cuboid(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Cuboid shape data
    Qt3DExtras::QCuboidMesh *cuboid = new Qt3DExtras::QCuboidMesh();

    // CuboidMesh Transform
    Qt3DCore::QTransform *cuboidTransform = new Qt3DCore::QTransform();
    cuboidTransform->setScale(1.0f);
    cuboidTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

    Qt3DExtras::QPhongAlphaMaterial  *cuboidMaterial = new Qt3DExtras::QPhongAlphaMaterial ();
    cuboidMaterial->setDiffuse(Qt::green);
    cuboidMaterial->setAlpha(0.5);

    //cuboid->attr

    //Cuboid
    {
        this->setMesh(cuboid);
        this->setMaterial(cuboidMaterial);
        this->setTransformation(cuboidTransform);
    }
}

bool Cuboid::intersectPoint(QVector3D &point)
{
    QVector3D PointDifCent = point-getPosition();
    QVector3D Scale = getTransformation()->scale3D()/2;
    return ((abs(PointDifCent.x()))<Scale.x()) && ((abs(PointDifCent.y()))<Scale.y()) && ((abs(PointDifCent.z()))<Scale.z());
}
