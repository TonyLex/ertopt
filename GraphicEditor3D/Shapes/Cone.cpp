#include "Cone.h"

Cone::Cone(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Cone shape data
    Qt3DExtras::QConeMesh *cone = new Qt3DExtras::QConeMesh();
    cone->setTopRadius(0.5);
    cone->setBottomRadius(1);
    cone->setLength(1);
    cone->setRings(50);
    cone->setSlices(20);

    // ConeMesh Transform
    Qt3DCore::QTransform *coneTransform = new Qt3DCore::QTransform();
    coneTransform->setScale(1.0f);
    coneTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 90.0f));
    //coneTransform->setTranslation(QVector3D(0.0f, 4.0f, -1.5));

    Qt3DExtras::QPhongAlphaMaterial *coneMaterial = new Qt3DExtras::QPhongAlphaMaterial();
    coneMaterial->setDiffuse(Qt::green);
    coneMaterial->setAlpha(0.5);

    // Cone
    {
        this->setMesh(cone);
        this->setMaterial(coneMaterial);
        this->setTransformation(coneTransform);
    }
}
