#include "Sphere.h"


Sphere::Sphere(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Sphere shape data
    Qt3DExtras::QSphereMesh *sphereMesh = new Qt3DExtras::QSphereMesh();
    sphereMesh->setRings(20);
    sphereMesh->setSlices(20);
    sphereMesh->setRadius(0.5);

    // Sphere mesh transform
    Qt3DCore::QTransform *sphereTransform = new Qt3DCore::QTransform();
    sphereTransform->setScale(1.0f);
    sphereTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

    Qt3DExtras::QPhongAlphaMaterial  *sphereMaterial = new Qt3DExtras::QPhongAlphaMaterial();
    sphereMaterial->setDiffuse(Qt::green);
    sphereMaterial->setAlpha(0.5);


    this->setTransformation(sphereTransform);
    this->setMaterial(sphereMaterial);
    this->setMesh(sphereMesh);

    this->setEnabled(true);

}

bool Sphere::intersectPoint(QVector3D &point)
{
    return (point.distanceToPoint(getPosition())<getTransformation()->scale3D().x()/2);
}
/*
void Sphere::handlePickerClicked(Qt3DRender::QPickEvent *pick)
{
    qDebug()<<"Sphere is pick";
}*/
