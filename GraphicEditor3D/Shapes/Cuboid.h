#ifndef CUBOID_H
#define CUBOID_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Cuboid: public Shape
{
     Q_OBJECT
public:
    Cuboid(Qt3DCore::QEntity *rootEntity);

    bool intersectPoint(QVector3D &point) override;
};

#endif // CUBOID_H
