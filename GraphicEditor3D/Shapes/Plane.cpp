#include "Plane.h"

Plane::Plane(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Plane shape data
    Qt3DExtras::QPlaneMesh *planeMesh = new Qt3DExtras::QPlaneMesh();
    planeMesh->setWidth(2);
    planeMesh->setHeight(2);

    // Plane mesh transform
    Qt3DCore::QTransform *planeTransform = new Qt3DCore::QTransform();
    planeTransform->setScale(1.3f);
    planeTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
    planeTransform->setTranslation(QVector3D(0.0f, -4.0f, 0.0f));

    Qt3DExtras::QPhongAlphaMaterial  *planeMaterial = new Qt3DExtras::QPhongAlphaMaterial ();
    planeMaterial->setDiffuse(Qt::green);
    planeMaterial->setAlpha(0.5);

    //Plane
    {
        this->setMesh(planeMesh);
        this->setMaterial(planeMaterial);
        this->setTransformation(planeTransform);
    }
}
