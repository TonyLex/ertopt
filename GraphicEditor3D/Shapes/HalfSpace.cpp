#include "HalfSpace.h"
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QMesh>

HalfSpace::HalfSpace(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Cuboid shape data
    Qt3DExtras::QCuboidMesh *cuboid = new Qt3DExtras::QCuboidMesh();

/*
    // Create a geometry renderer
    Qt3DRender::QMesh *cuboid = new Qt3DRender::QMesh();

    // Create a geometry
        Qt3DRender::QGeometry *geometry = new Qt3DRender::QGeometry();

        // Create a buffer for vertex positions
        Qt3DRender::QBuffer *vertexBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);

        QVector<float> vertices;
        int gridSize = 10;
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                // Add the vertex coordinates to the vector
                vertices.append(i);
                vertices.append(0);
                vertices.append(j);
            }
        }

        //qDebug()<< QByteArray((const char*)vertices.data(), vertices.size() * sizeof(float));

        vertexBuffer->setData(QByteArray((const char*)vertices.data(), vertices.size() * sizeof(float)));

        // Create a vertex attribute for positions
        Qt3DRender::QAttribute *positionAttribute = new Qt3DRender::QAttribute(geometry);
        positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
        positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
        positionAttribute->setVertexSize(3);
        positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        positionAttribute->setBuffer(vertexBuffer);
        positionAttribute->setByteStride(3 * sizeof(float));
        positionAttribute->setCount(3); // number of vertices

        geometry->addAttribute(positionAttribute);

        cuboid->setGeometry(geometry);

        QVector<float> normals;

        // populate vertices with vertex coordinates

        for (int i = 0; i < vertices.size() - 9; i += 9) {
            QVector3D v1(vertices[i], vertices[i + 1], vertices[i + 2]);
            QVector3D v2(vertices[i + 3], vertices[i + 4], vertices[i + 5]);
            QVector3D v3(vertices[i + 6], vertices[i + 7], vertices[i + 8]);
            QVector3D normal = QVector3D::normal(v1, v2, v3);
            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());
            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());
            normals.append(normal.x());
            normals.append(normal.y());
            normals.append(normal.z());
        }

        Qt3DRender::QBuffer *normalBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);

        normalBuffer->setData(QByteArray((const char*)normals.data(), normals.size() * sizeof(float)));
        // Create a vertex attribute for normals
        Qt3DRender::QAttribute *normalAttribute = new Qt3DRender::QAttribute(geometry);
        normalAttribute->setName(Qt3DRender::QAttribute::defaultNormalAttributeName());
        normalAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
        normalAttribute->setVertexSize(3);
        normalAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        normalAttribute->setBuffer(normalBuffer);
        normalAttribute->setByteStride(3 * sizeof(float));
        normalAttribute->setCount(3); // number of normals

        geometry->addAttribute(normalAttribute);
*/
    // CuboidMesh Transform
    Qt3DCore::QTransform *cuboidTransform = new Qt3DCore::QTransform();
    cuboidTransform->setScale(1.0f);
    cuboidTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

    Qt3DExtras::QPhongAlphaMaterial  *cuboidMaterial = new Qt3DExtras::QPhongAlphaMaterial ();
    cuboidMaterial->setDiffuse(Qt::green);
    cuboidMaterial->setAlpha(0.1);

    //cuboid->attr

    //Cuboid
    {
        this->setMesh(cuboid);
        this->setMaterial(cuboidMaterial);
        this->setTransformation(cuboidTransform);
    }
}

void HalfSpace::setParam(qreal xMin_l, qreal xMax_l, qreal yMin_l, qreal yMax_l, qreal zMin_l, qreal zMax_l)
{
    xMin = xMin_l;
    xMax = xMax_l;
    yMin = yMin_l;
    yMax = yMax_l;
    zMin = zMin_l;
    zMax = zMax_l;

    setScale(QVector3D(xMax-xMin, yMax-yMin, zMax-zMin));
    setPosition(QVector3D(xMin+(xMax-xMin)/2, yMin+(yMax-yMin)/2, zMin+(zMax-zMin)/2));

    //this->setPosition(QVector3D());
}

void HalfSpace::handlePickerClicked(Qt3DRender::QPickEvent *pick)
{
    qDebug()<<"intersect with HalfSpace";
}
