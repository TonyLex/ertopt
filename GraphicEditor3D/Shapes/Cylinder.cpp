#include "Cylinder.h"

Cylinder::Cylinder(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Cylinder shape data
    Qt3DExtras::QCylinderMesh *cylinder = new Qt3DExtras::QCylinderMesh();
    cylinder->setRadius(0.5);
    cylinder->setLength(1);
    cylinder->setRings(100);
    cylinder->setSlices(20);

    // CylinderMesh Transform
    Qt3DCore::QTransform *cylinderTransform = new Qt3DCore::QTransform();
    cylinderTransform->setScale(1);
    //cylinderTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
    cylinderTransform->setTranslation(QVector3D(0.0f, 0.0f, 0));

    Qt3DExtras::QPhongAlphaMaterial  *cylinderMaterial = new Qt3DExtras::QPhongAlphaMaterial ();
    cylinderMaterial->setDiffuse(Qt::green);
    cylinderMaterial->setAlpha(0.5);

    //Cylinder
    {
        this->setMesh(cylinder);
        this->setMaterial(cylinderMaterial);
        this->setTransformation(cylinderTransform);
    }
}
/*
void Cylinder::handlePickerClicked(Qt3DRender::QPickEvent *pick)
{
    qDebug()<<"Cylinder is selected";
}*/
