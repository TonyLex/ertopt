#include "Shape.h"

Shape::Shape(Qt3DCore::QEntity *rootEntity)
    :Qt3DCore::QEntity(rootEntity)
{
    transformation = new Qt3DCore::QTransform();

    transformation = new Qt3DCore::QTransform();
    material = new Qt3DExtras::QPhongAlphaMaterial ;
    mesh = new Qt3DRender::QGeometryRenderer();
    picker = new Qt3DRender::QObjectPicker(this);
    picker->setHoverEnabled(true);
    picker->setEnabled(true);

    this->addComponent(mesh);
    this->addComponent(transformation);
    this->addComponent(material);
    this->addComponent(picker);

    connect(picker, &Qt3DRender::QObjectPicker::clicked, this, &Shape::handlePickerClicked);

}

QVector3D Shape::getPosition() const
{
    return position;
}

Qt3DCore::QTransform *Shape::getTransformation() const
{
    return transformation;
}

void Shape::setTransformation(Qt3DCore::QTransform * transform)
{
    transformation = transform;
    this->addComponent(transformation);
}

void Shape::setPosition(QVector3D pos)
{
    position = pos;
    transformation->setTranslation(pos);
}

void Shape::setScale(QVector3D scale_l)
{
    scale = scale_l;
    transformation->setScale3D(scale);
}

void Shape::setMaterial(Qt3DExtras::QPhongAlphaMaterial  *material_l)
{
    material = material_l;
    this->addComponent(material);
}

void Shape::setMesh(Qt3DRender::QGeometryRenderer *mesh_l)
{
    mesh = mesh_l;
    this->addComponent(mesh);
}

void Shape::setPicker(Qt3DRender::QObjectPicker *picker_l)
{
    picker = picker_l;
}

void Shape::setSelected(bool selected_l)
{
    selection = selected_l;
    if(selection){
        emit signal_Parameters(position, scale, cond);
        material->setDiffuse(Qt::blue);
    }
    else    material->setDiffuse(Qt::green);
}

qreal Shape::getCond()
{
    return cond;
}

void Shape::setCond(qreal cond_l)
{
    cond = cond_l;
}

Qt3DRender::QGeometryRenderer *Shape::getMesh() const
{
    return mesh;
}

Qt3DExtras::QPhongAlphaMaterial *Shape::getMaterial()
{
    return material;
}

void Shape::handlePickerClicked(Qt3DRender::QPickEvent *pick)
{
    qDebug()<<" Shape clicked";

    emit signal_ItSelect(this);
    emit signal_Parameters(position, scale, cond);
    //pick->localIntersection()
}

void Shape::setParams(QVector3D pos_l, QVector3D scale_l, qreal cond_l)
{
    setPosition(pos_l);
    setScale(scale_l);
    setCond(cond_l);
}
