#include "ShapeManager.h"
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>

ShapeManager* ShapeManager::self = nullptr;

ShapeManager::ShapeManager()
{

}

ShapeManager *ShapeManager::getInstance()
{
    if(!self)
        self = new ShapeManager;
    return self;
}

void ShapeManager::getVerticlesAndNormals(Shape *shape, QVector<QVector3D> &vertices, QVector<QVector3D> &normals)
{
    // Получаем список атрибутов геометрии
    Qt3DRender::QGeometryRenderer *geometryRenderer = shape->getMesh();
    Qt3DCore::QTransform *transform = shape->getTransformation();
    const QVector<Qt3DRender::QAttribute *> attributes = geometryRenderer->geometry()->attributes();


    for (Qt3DRender::QAttribute *attribute : attributes) {
        qDebug()<< "attribute name: " << attribute->name();

/*
        if (attribute->attributeType() == Qt3DRender::QAttribute::IndexAttribute) {

            // Ищем атрибут с индексами вершин
            Qt3DRender::QAttribute *indexAttribute = attribute;

                // Получаем данные индексов

                Qt3DRender::QBuffer *buffer = indexAttribute->buffer();
                //buffer->setData(indexAttribute->buffer()->data());

                if (buffer) {
                        // If buffer is empty, use data-generator to make data available
                        if (buffer->data().isEmpty())
                            buffer->setData(buffer->dataGenerator()->operator()());
                        buffer->setSyncData(true);
                        buffer->setAccessType(Qt3DRender::QBuffer::AccessType::ReadWrite);
                }

                const char* indexData = buffer->data();//.constData();
                const int *indices = reinterpret_cast<const int *>(indexData);

                // Получаем количество индексов
                int indexCount = indexAttribute->count();

                for (int i = 0; i < indexCount; i++) {

                    const int x = *reinterpret_cast<const int *>(indexData + sizeof(int)*i);
                    qDebug()<<x;
                }


                qDebug()<<"indexCount = "<<indexCount;

                qDebug()<<" indexAttribute is =  " <<attribute->attributeType();
        }
*/
        if (attribute->name() == Qt3DRender::QAttribute::defaultPositionAttributeName()) {
            // Если атрибут - это атрибут позиции, то
            const Qt3DRender::QAttribute::VertexBaseType vertexBaseType = attribute->vertexBaseType();
            const int vertexSize = attribute->vertexSize();
            const int byteStride = attribute->byteStride();
            const int byteOffset = attribute->byteOffset();


            qDebug()<<"vertexSize = " << vertexSize;
            qDebug()<<"byteStride = " << byteStride;
            qDebug()<<"byteOffset = " << byteOffset;

            Qt3DRender::QBuffer *buffer = attribute->buffer();

            if (buffer) {
                    // If buffer is empty, use data-generator to make data available
                    if (buffer->data().isEmpty())
                        buffer->setData(buffer->dataGenerator()->operator()());
                    buffer->setSyncData(true);
                    buffer->setAccessType(Qt3DRender::QBuffer::AccessType::ReadWrite);
            }

            qDebug()<<"buffer->data() = "<<buffer->data().size();

            //const float *rawData = reinterpret_cast<const float*>() ;
            if (vertexBaseType == Qt3DRender::QAttribute::Float) {
                const char* data = buffer->data().constData();
                qDebug()<< "attribute count: " << attribute->count();

                for (int i = 0; i < attribute->count()/3; i++) {
                    //attribute->
                    const char* vertexData = data + byteOffset + (i) * byteStride;
                    //qDebug()<< vertexData;
                    const float x = *reinterpret_cast<const float *>(vertexData);
                    const float y = *reinterpret_cast<const float *>(vertexData + sizeof(float));
                    const float z = *reinterpret_cast<const float *>(vertexData + sizeof(float) * 2);
                    QVector3D vertex = QVector3D(x, y, z);
                    qDebug()<<vertex;
                    // Apply the transformation
                    vertex = transform->matrix() * vertex;
                    vertices.append(vertex);
                }
            }

        } else if (attribute->name() == Qt3DRender::QAttribute::defaultNormalAttributeName()) {
            // Если атрибут - это атрибут нормаль, то заполняем наш вектор нормалей


        }
    }
}

ShapeManager::~ShapeManager()
{

}

void ShapeManager::AddShape(Shape *shape)
{
    qDebug()<< "ShapeManager::AddShape";
    ShapesList.append(shape);

    connect(shape, &Shape::signal_ItSelect, SelectionShape::getInstanse(), &SelectionShape::Select);
    emit signal_NewShape(shape);

}

void ShapeManager::DeleteShape(Shape *shape)
{
    SelectionShape::getInstanse()->DeselectAll();
    ShapesList.removeOne(shape);
    delete shape;

}

QList<Shape *> ShapeManager::getShapesList()
{
    return ShapesList;
}


