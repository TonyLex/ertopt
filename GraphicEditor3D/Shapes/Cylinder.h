#ifndef Cylinder_H
#define Cylinder_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Cylinder: public Shape
{
     Q_OBJECT
public:
    Cylinder(Qt3DCore::QEntity *rootEntity);

public slots:
    //virtual void handlePickerClicked(Qt3DRender::QPickEvent *pick);
};

#endif // Cylinder_H
