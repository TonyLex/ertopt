#ifndef SHAPE_H
#define SHAPE_H

#include <QVector3D>
#include <QtCore/QObject>
#include <Qt3DCore/qentity.h>
#include <Qt3DCore/qtransform.h>
#include <Qt3DExtras/QPhongAlphaMaterial >
#include <QGeometryRenderer>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QConeMesh>
#include <Qt3DExtras/QTorusMesh>
#include <Qt3DExtras/QPlaneMesh>

#include <Qt3DExtras/QPhongAlphaMaterial>
#include <Qt3DExtras/QNormalDiffuseSpecularMapMaterial>
#include <Qt3DRender/QObjectPicker>

#include <Qt3DRender/QPickEvent>
#include <Qt3DRender/QLayer>
#include <Qt3DRender/QLayerFilter>
#include <Qt3DRender/qcamera.h>


class Shape: public Qt3DCore::QEntity
{
    Q_OBJECT
public:
    Shape(Qt3DCore::QEntity *rootEntity);

    /// Положение фигуры в глобальной системе координат
    QVector3D getPosition() const;

    /// Возращает трансформацию для фигуры
    Qt3DCore::QTransform * getTransformation() const;

    /// Устанавливает трансформацию для фигуры
    void setTransformation(Qt3DCore::QTransform *transform);

    /// Установка позиции фигуры в глобальные координаты
    void setPosition(QVector3D pos);

    void setScale(QVector3D scale);

    void setMaterial(Qt3DExtras::QPhongAlphaMaterial  * material_l);

    void setMesh(Qt3DRender::QGeometryRenderer *mesh_l);

    void setPicker(Qt3DRender::QObjectPicker *picker);

    void setSelected(bool selected_l);

    qreal getCond();

    void setCond(qreal cond_l);

    Qt3DRender::QGeometryRenderer * getMesh() const;

    Qt3DExtras::QPhongAlphaMaterial *getMaterial();

    virtual bool intersectPoint(QVector3D &point){ return false; };

signals:
    void signal_Parameters(QVector3D pos, QVector3D width, qreal cond);
    void signal_ItSelect(Shape *shape);

public slots:
    virtual void handlePickerClicked(Qt3DRender::QPickEvent *pick);
    void setParams(QVector3D pos, QVector3D width, qreal cond);

private:
    /// Проводимость
    qreal cond = 0;

    /// Положение фигуры
    QVector3D position;

    /// Ширина по X, Y, Z
    QVector3D scale = {1,1,1};

    Qt3DCore::QTransform *transformation;
    Qt3DExtras::QPhongAlphaMaterial *material;
    Qt3DRender::QGeometryRenderer *mesh;
    Qt3DRender::QObjectPicker *picker;
    Qt3DRender::QCamera *camera;

    bool selection = false;
};

#endif // SHAPE_H
