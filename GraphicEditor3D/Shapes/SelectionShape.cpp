#include "SelectionShape.h"

SelectionShape * SelectionShape::self = nullptr;

SelectionShape *SelectionShape::getInstanse()
{
    if (!self)
        self = new SelectionShape;
    return self;
}

SelectionShape::SelectionShape(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{

}

void SelectionShape::Select(Shape *shape)
{
    qDebug()<< "SelectionShape::Select "<< sender();

    DeselectAll();
    SelectionShapesList.append(shape);
    shape->setSelected(true);
    emit signal_selectionChange();
}

void SelectionShape::DeSelect(Shape *shape)
{
    SelectionShapesList.removeAll(shape);
    emit signal_selectionChange();
}

void SelectionShape::DeselectAll()
{
    for(auto &shape:SelectionShapesList)
        shape->setSelected(false);

    SelectionShapesList.clear();
    emit signal_selectionChange();
}

QList<Shape *> SelectionShape::getSelectionShapesList()
{
    return SelectionShapesList;
}
