#ifndef Cone_H
#define Cone_H

#include <GraphicEditor3D/Shapes/Shape.h>

class Cone: public Shape
{
     Q_OBJECT
public:
    Cone(Qt3DCore::QEntity *rootEntity);
};

#endif // Cone_H
