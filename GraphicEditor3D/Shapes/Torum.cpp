#include "Torum.h"

Torum::Torum(Qt3DCore::QEntity *rootEntity)
    :Shape(rootEntity)
{
    // Torus shape data
    Qt3DExtras::QTorusMesh *m_torus = new Qt3DExtras::QTorusMesh();
    m_torus->setRadius(1.0f);
    m_torus->setMinorRadius(0.4f);
    m_torus->setRings(100);
    m_torus->setSlices(20);

    // TorusMesh Transform
    Qt3DCore::QTransform *torusTransform = new Qt3DCore::QTransform();
    torusTransform = new Qt3DCore::QTransform();
    torusTransform->setScale(2.0f);
    torusTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 1.0f, 0.0f), 25.0f));
    torusTransform->setTranslation(QVector3D(5.0f, 4.0f, 0.0f));

    Qt3DExtras::QPhongAlphaMaterial  *torusMaterial = new Qt3DExtras::QPhongAlphaMaterial ();
    torusMaterial->setDiffuse(Qt::green);
    torusMaterial->setAlpha(0.5);

    //Torus
    {
        this->setMesh(m_torus);
        this->setMaterial(torusMaterial);
        this->setTransformation(torusTransform);
    }
}
