#ifndef HALFSPACE_H
#define HALFSPACE_H

#include <GraphicEditor3D/Shapes/Cuboid.h>

class HalfSpace: public Shape
{
public:
    HalfSpace(Qt3DCore::QEntity *rootEntity);

    void setParam(qreal xMin, qreal xMax, qreal yMin, qreal yMax, qreal zMin, qreal zMax);

    void handlePickerClicked(Qt3DRender::QPickEvent *pick) override;

private:
    int xMin = -10;
    int xMax = 10;
    int yMin = -10;
    int yMax = 10;
    int zMin = -10;
    int zMax = 10;
};

#endif // HALFSPACE_H
