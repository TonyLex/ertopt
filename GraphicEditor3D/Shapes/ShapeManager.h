#ifndef SHAPEMANAGER_H
#define SHAPEMANAGER_H

#include "Shape.h"
#include "SelectionShape.h"

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QDebug>
#include <Qt3DRender>

class ShapeManager: public QObject
{
    Q_OBJECT
public:
    ShapeManager();
    ~ShapeManager();
    void AddShape(Shape *shape);
    void DeleteShape(Shape *shape);

    QList<Shape *> getShapesList();

    static ShapeManager* getInstance();

    void getVerticlesAndNormals(Shape * shape, QVector<QVector3D> &vert, QVector<QVector3D> &norm);

signals:
    void signal_NewShape(Shape * shape);

private:
    static ShapeManager* self;
    QList<Shape *> ShapesList;
};

#endif // SHAPEMANAGER_H
