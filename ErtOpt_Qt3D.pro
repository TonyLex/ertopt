android|ios|tvos|winrt {
    warning( "This example is not supported for android, ios, tvos, or winrt." )
}

TEMPLATE = app

CONFIG += resources_big

target.path = $$[QT_INSTALL_EXAMPLES]/qt3d/$$TARGET
INSTALLS += target

QT       += core gui
QT += 3dcore 3drender 3dinput 3dextras
QT += widgets



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CommonGui/CommonButton.cpp \
    Files/STLWriter.cpp \
    Files/SaveToPolyFile.cpp \
    GraphicEditor3D/GraphEditorWidget.cpp \
    GraphicEditor3D/Shapes/Cone.cpp \
    GraphicEditor3D/Shapes/Cuboid.cpp \
    GraphicEditor3D/Shapes/Cylinder.cpp \
    GraphicEditor3D/Shapes/HalfSpace.cpp \
    GraphicEditor3D/Shapes/Plane.cpp \
    GraphicEditor3D/Shapes/SelectionShape.cpp \
    GraphicEditor3D/Shapes/Shape.cpp \
    GraphicEditor3D/Shapes/ShapeManager.cpp \
    GraphicEditor3D/Shapes/Sphere.cpp \
    GraphicEditor3D/Shapes/Torum.cpp \
    GraphicEditor3D/Window3DCustom.cpp \
    GraphicEditor3D/scenemodifier.cpp \
    Grid/Grid.cpp \
    Grid/Node.cpp \
    MainWidget.cpp \
    Settings/SettingsWidget.cpp \
    Solver/SolveDirectProblemFDM.cpp \
    Solver/SolveDirectProblemFEM.cpp \
    Surface/Surface.cpp \
    main.cpp \
    tetgen/predicates.cxx \
    tetgen/tetgen.cxx

HEADERS += \
    CommonGui/CommonButton.h \
    Files/STLWriter.h \
    Files/SaveToPolyFile.h \
    GraphicEditor3D/GraphEditorWidget.h \
    GraphicEditor3D/Shapes/Cone.h \
    GraphicEditor3D/Shapes/Cuboid.h \
    GraphicEditor3D/Shapes/Cylinder.h \
    GraphicEditor3D/Shapes/HalfSpace.h \
    GraphicEditor3D/Shapes/Plane.h \
    GraphicEditor3D/Shapes/SelectionShape.h \
    GraphicEditor3D/Shapes/Shape.h \
    GraphicEditor3D/Shapes/ShapeManager.h \
    GraphicEditor3D/Shapes/Sphere.h \
    GraphicEditor3D/Shapes/Torum.h \
    GraphicEditor3D/Window3DCustom.h \
    GraphicEditor3D/scenemodifier.h \
    Grid/Grid.h \
    Grid/Node.h \
    MainWidget.h \
    Settings/SettingsWidget.h \
    Solver/SolveDirectProblemFDM.h \
    Solver/SolveDirectProblemFEM.h \
    Surface/Surface.h \
    tetgen/tetgen.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    GraphicEditor3D/Images.qrc \
    GraphicEditor3D/Images.qrc \
    Icons.qrc
