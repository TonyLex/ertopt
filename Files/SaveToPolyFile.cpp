#include "SaveToPolyFile.h"


//#include <tetgen/tetgen.cxx>


SaveToPolyFile::SaveToPolyFile(QWidget *parrent_l)
{
    parrent = parrent_l;
}

void SaveToPolyFile::savePoly(Shape* shape, QString fileName)
{

    // Создаем вектор для хранения узлов
    QVector<QVector3D> vertices;
    // Создаем вектор для хранения фасет
    QVector<QVector3D> facets;
    // Создаем вектор для хранения дырок
    QVector<QVector3D> holes;
    // Создаем вектор для хранения регионов
    QVector<QVector3D> regions;

    ShapeManager::getInstance()->getVerticlesAndNormals(shape, vertices, facets);

    //-------------------------------------------------------------------------------------------
/*
    int numPoints = vertices.size();
    int numFaces = vertices.size();

    tetgenio in, out1;

    // Заполнение координат вершин
    in.firstnumber = 0; // Номерация вершин начинается с 0
    in.numberofpoints = numPoints;
    in.pointlist = new double[in.numberofpoints * 3];
    for (int i = 0; i < numPoints; i++) {
        in.pointlist[i * 3] = vertices[i].x();
        in.pointlist[i * 3 + 1] = vertices[i].y();
        in.pointlist[i * 3 + 2] = vertices[i].z();
    }

    // Заполнение граней
    in.numberoffacets = numFaces;
    in.facetlist = new tetgenio::facet[in.numberoffacets];
    in.facetmarkerlist = new int[in.numberoffacets];
    for (int i = 0; i < numFaces; i++) {
        tetgenio::facet& f = in.facetlist[i];
        f.numberofpolygons = 1;
        f.polygonlist = new tetgenio::polygon[f.numberofpolygons];
        f.numberofholes = 0;
        f.holelist = NULL;
        tetgenio::polygon& p = f.polygonlist[0];
        //p.numberofvertices = face[i].size();
        p.vertexlist = new int[p.numberofvertices];
        for (int j = 0; j < p.numberofvertices; j++) {
            //p.vertexlist[j] = face[i][j];
        }
        in.facetmarkerlist[i] = i + 1;
    }

    // заполнение массива вершин
    //double vertexArray[3*numPoints];
    // заполнение массива ребер
    //int edgeArray[2*numPoints];

    // Запуск генерации сетки
    tetgenbehavior b;// = new tetgenbehavior();
    b.plc = 1; // генерировать замкнутую сетку
    b.quality = 1; // генерировать сетку с качеством
    b.minratio = 0.1; // минимальное отношение диагонали
    b.mindihedral = 10; // минимальный угол между ребрами

    tetrahedralize(&b, &in, &out1);
    QString fname_node = (fileName + QString(".node"));
    QString fname_elements = (fileName + QString(".ele"));
    QString fname_faces = (fileName + QString(".face"));
    out1.save_nodes((const char*)fname_node.data());
    out1.save_elements((const char*)fname_elements.data());
    out1.save_faces((const char*)fname_faces.data());
*/
    //-------------------------------------------------------------------------------------------


    // Открываем файл для записи
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "Could not open file for writing";
        return;
    }
    QTextStream out(&file);

    // Записываем информацию об узлах в файл
    out << "# part 1 - node list\n#  " << vertices.size() << " nodes, 3 dim., no attr. no mark\n"<< vertices.size() << "  3  0  0\n\n";
    for(int i=0; i<vertices.size(); i++) {
            out << i << " " << vertices[i].x() << " " << vertices[i].y() << " " << vertices[i].z()<<endl;
        }
        // Записываем информацию о фасетах в файл
        out << "# part 2 - facet list\n#  " << facets.size() << " facets, no mark\n" << facets.size() << "  0\n\n";
        for(int i=0; i<facets.size(); i++) {
            out << "1\n3  " << facets[i].x() << " " << facets[i].y() << " " << facets[i].z()<<endl;
        }
        // Записываем информацию о дырках в файл
        out << "# part 3 - hole list\n" << holes.size() << "\n\n";
        for(int i=0; i<holes.size(); i++) {
            out << holes[i].x() << " " << holes[i].y() << " " << holes[i].z()<<endl;
        }
        // Записываем информацию о регионах в файл
        out << "# part 4 - region list\n" << regions.size() << "\n\n";
        for(int i=0; i<regions.size(); i++) {
            out << regions[i].x() << " " << regions[i].y() << " " << regions[i].z()<< " 1"<<endl;
        }
        file.close();
        qDebug() << "Data saved to " << fileName;
}

void SaveToPolyFile::SaveFileAs()
{
    QString fname = QFileDialog::getSaveFileName(parrent, "test", ".", "3D Format (*.poly);; STL files (*.stl);; Text files (*.txt)" );
    qDebug() << "name is : " << fname;

    if(fname.endsWith(".poly")){
       qDebug()<<"file name endsWith .poly";
       QList<Shape *> shape_list = ShapeManager::getInstance()->getShapesList();
       for(auto &shape:shape_list)
           savePoly(shape, fname);

    } else if(fname.endsWith(".stl")){
       qDebug()<<"file name endsWith .stl";
       STLWriter stl_writer;
       QVector<QVector3D> verts;
       QVector<QVector3D> norms;

       QList<Shape *> shape_list = ShapeManager::getInstance()->getShapesList();
       for(auto &shape:shape_list){
           ShapeManager::getInstance()->getVerticlesAndNormals(shape, verts, norms);
           stl_writer.addVertex(verts);
           stl_writer.addVertex(norms);

           stl_writer.saveToFile(fname);
       }

           //saveFigure(shape->getMesh(), fname);
    }

}
