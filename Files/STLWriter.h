#ifndef STLWRITER_H
#define STLWRITER_H

#include <QFile>
#include <QTextStream>
#include <QVector3D>
#include <QVector>
#include <QDataStream>

class STLWriter
{
public:
    STLWriter();

    void addVertex(QVector<QVector3D> &vertex);
    //void addNormal(QVector<QVector3D> &normal);
    void saveToFile(const QString &fileName);

private:
    QVector<QVector3D> m_vertices;
    //QVector<QVector3D> m_normals;
};

#endif // STLWRITER_H
