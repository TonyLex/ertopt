#include "STLWriter.h"


STLWriter::STLWriter()
{

}

void STLWriter::addVertex(QVector<QVector3D> &vertex)
{
    m_vertices.append(vertex);
}

/*
void STLWriter::addNormal(QVector<QVector3D> &normal)
{
    m_normals.append(normal);
}
*/

void STLWriter::saveToFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return;

    QDataStream out(&file);
    out.setByteOrder(QDataStream::LittleEndian);

    // Write the header
    char header[80] = {0};
    out.writeRawData(header, 80);

    // Write the number of triangles
    quint32 nTriangles = m_vertices.size() / 3;
    out << nTriangles;

    // Write the triangles
    for (int i = 0; i < m_vertices.size()-3; i += 3) {
        QVector3D normal = QVector3D::normal(m_vertices[i], m_vertices[i+1], m_vertices[i+2]);
        out << normal << m_vertices[i] << m_vertices[i+1] << m_vertices[i+2];
        quint16 attributeByteCount = 0;
        out << attributeByteCount;
    }

    file.close();
}
