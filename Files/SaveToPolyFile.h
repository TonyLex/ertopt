#ifndef SAVETOPOLYFILE_H
#define SAVETOPOLYFILE_H

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <Qt3DRender>

#include <QObject>
#include <QWidget>
#include <QFileDialog>
#include <GraphicEditor3D/Shapes/ShapeManager.h>
#include <Files/STLWriter.h>

#include <tetgen/tetgen.h>

class SaveToPolyFile: public QObject
{
    Q_OBJECT
public:
    SaveToPolyFile(QWidget *parrent = nullptr);

    void savePoly(Shape* shape, QString fileName);

public slots:
    void SaveFileAs();

private:
    QWidget *parrent;

    template<typename T>
    QVector<QVector3D> QByteArrayToVector(QByteArray data) {
        const T *rawData = reinterpret_cast<const T*>(data.constData()) ;
        int size = data.size() / sizeof(T);
        QVector<QVector3D> result;
        for (int i = 0; i < size; i+=3) {
            result.append(QVector3D(rawData[i], rawData[i+1], rawData[i+2]));
        }
        return result;
    }
};

#endif // SAVETOPOLYFILE_H
