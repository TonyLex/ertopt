#include "MainWidget.h"

#include <QApplication>
#include <Solver/SolveDirectProblemFEM.h>
#include <QDebug>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    MainWidget w;
    w.show();
    return a.exec();

/*
    SolveDirectProblemFEM sdp(10, 10, 10, 10, 10, 10);
    vector<BoundaryCondition> bcs = { {0, 1, 0}, {100, 2, 0}, {200, 3, 0} };
    sdp.setBoundaryConditions(bcs);
    VectorXd potential = sdp.solve(1);
    cout << "Potential: " << endl << potential << endl;
    return 0;
*/
}
